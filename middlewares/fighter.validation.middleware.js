const { fighter } = require('../models/fighter');
const { statusCodes } = require('../config/constants/statusCodes');
const { FighterService } = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
    try {
        Object.assign(req, checkFighterUniqueness(req));

        if (!req.error) {
            Object.assign(req, fighterValidation(req));
        }
    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

const updateFighterValid = (req, res, next) => {
    if (FighterService.search({ id: req.params.id })) {
        Object.assign(req, checkFighterUniqueness(req));
        if (!req.error) {
            Object.assign(req, fighterValidation(req));
        }
    } else {
        req.error = true;
        req.status = statusCodes.notFound;
        req.message = 'Fighter not found';
    }
    try {

    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

function fighterValidation(req) {
    let validationErrors = [];
    const inputInfo = req.body;
    console.log(inputInfo)

    Object.keys(inputInfo).forEach((inputKey) => {
        if (!fighter[inputKey] || inputKey === 'id') {
            validationErrors.push(`validation: ${inputKey} is extra property`);
        } else {
            Object.keys(fighter[inputKey]).forEach((fighterKey) => {
                switch (fighterKey) {
                    case 'required': {
                        if (!inputInfo[inputKey] && !fighter[inputInfo].defaultValue) {
                            validationErrors.push(`validation: ${inputKey} is required`);
                        }
                        break;
                    }

                    case 'type': {
                        if(inputKey == 'name') console.log(typeof inputInfo[inputKey], fighter[inputKey].type, fighter[inputKey].type)
                        if (!(fighter[inputKey].type === 'number' && typeof +inputInfo[inputKey] === fighter[inputKey].type)
                            || !(typeof inputInfo[inputKey] === fighter[inputKey].type && fighter[inputKey].type !== 'number')) {
                            validationErrors.push(`validation: ${inputKey} - wrong type`);
                        }
                        break;
                    }

                    case 'minLength': {
                        consla.log(inputInfo[inputKey].length, fighter[inputKey].minLength)
                        if (inputInfo[inputKey].length < fighter[inputKey].minLength) {
                            validationErrors.push(`validation: ${inputKey} is shorter than ${fighter[inputKey].minLength}`);
                        }
                        break;
                    }

                    case 'maxLength': {
                        if (inputInfo[inputKey].length > fighter[inputKey].maxLength) {
                            validationErrors.push(`validation: ${inputKey} is longer than ${fighter[inputKey].maxLength}`);
                        }
                        break;
                    }

                    case 'minValue': {
                        if (inputInfo[inputKey] < fighter[inputKey].minLength) {
                            validationErrors.push(`validation: ${inputKey} is smaller than ${fighter[inputKey].minLength}`);
                        }
                        break;
                    }

                    case 'maxValue': {
                        if (inputInfo[inputKey] > fighter[inputKey].maxLength) {
                            validationErrors.push(`validation: ${inputKey} is bigger than ${fighter[inputKey].maxLength}`);
                        }
                        break;
                    }

                    case 'defaultValue': {
                        if (!inputInfo[inputKey]) {
                            validationErrors.push(`validation: ${inputKey} is required`);
                        }
                        break;
                    }

                    default: break;
                }
            });
        }
    });

    if (validationErrors.length > 0) {
        req.status = statusCodes.requestError;
        req.error = true;
        req.message = validationErrors;
    }
    return req;
}

function checkFighterUniqueness(req) {
    const id = req.params.id;
	const isNameUnique = !FighterService.search({ name: req.body.name }).name
		|| FighterService.search({ name: req.body.name }).name === FighterService.search({ id }).name;

	if (!isNameUnique) {
        req.error = true;
		req.status = statusCodes.requestError;
		req.message = 'uniqueness: name is busy';
	} 

    return req;
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;