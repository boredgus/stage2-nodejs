const responseMiddleware = (req, res, next) => {
	// TODO: Implement middleware that returns result of the query
	try {
		if (req.error) {
			res.status(req.status).send({
				error: true,
				message: req.message
			});
			console.log(req.message);
		} else {
			res.status(200).send({
				response: req.response,
				message: req.message,
			});
		}
	} catch (err) {
		res.err = err;
	} finally {
		next();
	}
}

exports.responseMiddleware = responseMiddleware;