const { user } = require('../models/user');
const { statusCodes } = require('../config/constants/statusCodes');
const UserService = require('../services/userService');
const createUserValid = (req, res, next) => {
	// TODO: Implement validatior for user entity during creation

	try {
		Object.assign(req, checkUserUniqueness(req));

		if (!req.error) {
			Object.assign(req, userValidation(req));
		}
	} catch (err) {
		res.err = err;
	} finally {
		next();
	}
}

const updateUserValid = (req, res, next) => {
	// TODO: Implement validatior for user entity during update

	try {
		if (UserService.search({ id: req.params.id })) {
			Object.assign(req, checkUserUniqueness(req));
			if (!req.error) {
				Object.assign(req, userValidation(req));
			}
		} else {
			req.error = true;
			req.status = statusCodes.notFound;
			req.message = 'User not found';
		}
	} catch (err) {
		res.err = err;
	} finally {
		next();
	}
}

function userValidation(req) {
	const inputInfo = req.body;
	let validationErrors = [];

	Object.keys(inputInfo).forEach((inputKey) => {
		console.log(inputKey)
		if (!user[inputKey] || inputKey === 'id') {
			validationErrors.push(`validation: ${inputKey} is extra property`);
		} else {
			Object.keys(user[inputKey]).forEach((userKey) => {
				switch (userKey) {
					case 'required': {
						if (!inputInfo[inputKey]) {
							validationErrors.push(`validation: ${inputKey} is required`);
						}
						break;
					}

					case 'type': {
						if (typeof inputInfo[inputKey] !== user[inputKey].type) {
							validationErrors.push(`validation: ${inputKey} - wrong type`);
						}
						break;
					}

					case 'pattern': {
						if (!((new RegExp(user[inputKey].pattern)).test(inputInfo[inputKey]))) {
							validationErrors.push(`validation: ${inputKey} - wrong pattern`);
						}
						break;
					}

					case 'minLength': {
						if (inputInfo[inputKey].length < user[inputKey].minLength) {
							validationErrors.push(`validation: ${inputKey} is shorter than ${user[inputKey].minLength}`);
						}
						break;
					}

					case 'maxLength': {
						if (inputInfo[inputKey].length > user[inputKey].maxLength) {
							validationErrors.push(`validation: ${inputKey} is longer than ${user[inputKey].maxLength}`);
						}
						break;
					}

					default: break;
				}
			});
		}
	});

	if (validationErrors.length > 0) {
		req.status = statusCodes.requestError;
		req.error = true;
		req.message = validationErrors;
	}

	return req;
}

function checkUserUniqueness(req) {
	const id = req.params.id;
	const isPhoneNumberUnique = !UserService.search({ phoneNumber: req.body.phoneNumber }).phoneNumber
		|| UserService.search({ phoneNumber: req.body.phoneNumber }).phoneNumber === UserService.search({ id }).phoneNumber;
	const isEmailUnique = !UserService.search({ email: req.body.email }).email
		|| UserService.search({ email: req.body.email }).email === UserService.search({ id }).email;
	let uniquenessErrors = [];

	if (!isPhoneNumberUnique) {
		uniquenessErrors.push('uniqueness: phone number is busy');
	}

	if (!isEmailUnique) {
		uniquenessErrors.push('uniqueness: email is busy');
	}

	if (uniquenessErrors.length > 0) {
		req.error = true;
		req.status = statusCodes.requestError;
		req.message = uniquenessErrors;
	}
	return req;
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;