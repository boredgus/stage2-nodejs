exports.fighter = {
    id: {
        type: 'string',
        required: false
    },
    name: {
        type: 'string',
        required: true,
        minLength: 2,
        maxLength: 25
    },
    health: {
        type: 'number',
        required: false,
        minValue: 80,
        maxValue: 120,
        defaultValue: 100
    },
    power: {
        type: 'number',
        required: true,
        minValue: 1,
        maxValue: 100
    },
    defense: {
        type: 'number',
        required: true,
        minValue: 1,
        maxValue: 10
    }, 
}