exports.user = {
    id: {
        type: 'string',
        required: false
    },
    firstName: {
        type: 'string',
        required: true,
        minLength: 2,
        maxLength: 15
    },
    lastName: {
        type: 'string',
        required: true,
        minLength: 2,
        maxLength: 20
    },
    email: {
        type: 'string',
        required: true,
        pattern: /^[A-Za-z0-9]{1,}(\.?[A-Za-z0-9]){0,}@gmail\.com$/
    },
    phoneNumber: {
        type: 'string',
        required: true,
        pattern: /^\+380[0-9]{9}/
    },
    password: {
        type: 'string',
        required: true,
        pattern: /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/,
        minLength: 3,
        maxLength: 25
    }
}