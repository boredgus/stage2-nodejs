const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');
const { statusCodes } = require('../config/constants/statusCodes');

const router = Router();

router.get('/',
	(req, res, next) => {
		try {
			req.response = FighterService.getAll();
			req.message = 'Get all fighters';
		} catch (err) {
			res.err = err;
		} finally {
			next();
		}
	},
	responseMiddleware
);

router.get('/:id',
	(req, res, next) => {
		try {
			if (!req.error) {
				const id = req.params.id;
				const fighter = FighterService.search({ id });
				if (fighter) {
					req.message = `Get fighter ${id}`;
					req.response = fighter;
				} else {
					req.error = true;
					req.status = statusCodes.notFound;
					req.message = 'Fighter not found';
				}
			}
		} catch (err) {
			res.send(err);
		} finally {
			next();
		}
	},
	responseMiddleware
);

router.post('/',
	createFighterValid,
	(req, res, next) => {
		try {
			console.log(req.error)
			if (!req.error) {
				FighterService.create(req.body);
				req.message = 'Fighter has created';
			}
		} catch (err) {
			res.send(err);
		} finally {
			next();
		}
	},
	responseMiddleware
);

router.put('/:id',
	updateFighterValid,
	(req, res, next) => {
		try {
			if (!req.error) {
				const id = req.params.id;
				if (FighterService.search({ id })) {
					FighterService.update(id, req.body);
					req.message = `Fighter ${id} updated`;
				} else {
					req.error = true;
					req.status = statusCodes.notFound;
					req.message = 'Fighter not found';
				}
			}
		} catch (err) {
			res.send(err);
		} finally {
			next();
		}
	},
	responseMiddleware
);

router.delete('/:id',
	(req, res, next) => {
		try {
			if (!req.error) {
				const id = req.params.id;

				if (FighterService.search({ id })) {
					FighterService.delete(id);
					req.message = `Fighter ${id} deleted`;
				} else {
					req.error = true;
					req.status = statusCodes.notFound;
					req.message = 'Fighter not found';
				}
			}
		} catch (err) {
			res.send(err);
		} finally {
			next();
		}
	},
	responseMiddleware
);
module.exports = router;