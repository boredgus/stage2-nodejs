const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { statusCodes } = require('../config/constants/statusCodes');

const router = Router();

router.get('/',
	(req, res, next) => {
		try {
			req.response = UserService.getAll();
			req.message = 'Get all users';
		} catch (err) {
			res.err = err;
		} finally {
			next();
		}
	},
	responseMiddleware
);

router.get('/:id',
	(req, res, next) => {
		try {
			if (!req.error) {
				const id = req.params.id;
				const user = UserService.search({ id });
				if (user) {
					req.message = `Get user ${id}`;
					req.response = user;
				} else {
					req.error = true;
					req.status = statusCodes.notFound;
					req.message = 'User not found';
				}
			}
		} catch (err) {
			res.err = err;
		} finally {
			next();
		}
	},
	responseMiddleware
);

router.post('/',
	createUserValid,
	(req, res, next) => {
		try {
			if (!req.error) {
				UserService.create(req.body);
				req.message = 'User has created';
			}
		} catch (err) {
			res.err = err;
		} finally {
			next();
		}
	},
	responseMiddleware
);

router.put('/:id',
	updateUserValid,
	(req, res, next) => {
		try {
			if (!req.error) {
				const id = req.params.id;
				
				UserService.update(id, req.body);
				req.message = `User ${id} updated`;
			}
		} catch (err) {
			res.err = err;
		} finally {
			next();
		}
	},
	responseMiddleware
);

router.delete('/:id',
	(req, res, next) => {
		try {
			if (!req.error) {
				const id = req.params.id;

				if (UserService.search({ id })) {
					UserService.delete(id);
					req.message = `User ${id} deleted`;
				} else {
					req.error = true;
					req.status = statusCodes.notFound;
					req.message = 'User not found';
				}
			}
		} catch (err) {
			res.err = err;
		} finally {
			next();
		}
	},
	responseMiddleware
);

module.exports = router;