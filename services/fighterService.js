const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    getAll() {
        return FighterRepository.getAll();
    }

    create(user) {
        FighterRepository.create(user);
    }

    update(id, fighterData) {
        FighterRepository.update(id, fighterData);
    }

    delete(id) {
        FighterRepository.delete(id);
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();