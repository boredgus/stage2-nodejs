const { UserRepository } = require('../repositories/userRepository');

class UserService {
    getAll() {
        return UserRepository.getAll();
    }

    create(user) {
        UserRepository.create(user);
    }

    update(id, userData) {
        UserRepository.update(id, userData);
    }

    delete(id) {
        UserRepository.delete(id);
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();